# Bazoo backend

This repository contains backend code for 'Bazoo' e-commerce website. Created using NodeJS server, TypeScript and Mongo database.

## Live Demo:

- [Bazoo](https://teal-dasik-cf9484.netlify.app/)
