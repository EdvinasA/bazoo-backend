import { Router, Request, Response } from 'express';

const router = Router();

router.get('/cart', async (req: Request, res: Response) => {
  res.send('Express + TypeScript Server from ./routes/cart folder');
});

export = router;
