import { Router } from 'express';
import {
  getAllProducts,
  getSingleProduct,
  getFeaturedProducts,
  filterProducts,
  searchProducts,
} from '../controller/products.controller';

const router = Router();
const routePrefix: string = '/products';

//---- return all products route ----//
router.get(`${routePrefix}/all`, getAllProducts);
//---- return featured products route ----//
router.get(`${routePrefix}/featured`, getFeaturedProducts);
//---- products filter route ----//
router.post(`${routePrefix}/filtered`, filterProducts);
//---- products search route ----//
router.post(`${routePrefix}/search`, searchProducts);
//---- return single product route ----//
router.get(`${routePrefix}/:slug`, getSingleProduct);

export = router;
