import mongoose from 'mongoose';

const ProductSchema = new mongoose.Schema({
  slug: {
    type: String,
    rrequired: true,
  },
  images: {
    type: Array,
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  price: {
    type: String,
    required: true,
  },
  oldPrice: {
    type: String,
    required: true,
  },
  category: {
    type: String,
    required: true,
  },
  colors: {
    type: [String],
    required: true,
  },
  quantity: {
    type: Number,
    required: true,
  },
  gender: {
    type: [String],
    required: true,
  },
  size: {
    type: [String],
    required: true,
  },
  rating: {
    type: Number,
    required: true,
  },
});

ProductSchema.index({ '$**': 'text' });
const ProductModel = mongoose.model('Product', ProductSchema);
export default ProductModel;
