import { Request, Response } from 'express';
import Product from '../model/product.model';
import { filterByPriceRange } from '../helpers/filterByPraceRange';
import { PriceRange } from '../types/types';

//---- get all prducts from database ----//
export const getAllProducts = async (request: Request, response: Response) => {
  try {
    const products = await Product.find();
    if (!products.length)
      return response.status(404).json({ message: 'No products found!' });
    response.status(200).json(products);
  } catch (error) {
    if (error instanceof Error)
      response.status(500).json({ error: 'Internal Server Error.' });
  }
};
//---- get single product from database ----//
export const getSingleProduct = async (
  request: Request,
  response: Response
) => {
  const { slug } = request.params;
  try {
    const product = await Product.findOne({ slug: slug });
    if (!product)
      return response.status(404).json({ message: 'No product found!' });
    response.status(200).json(product);
  } catch (error) {
    if (error instanceof Error)
      response.status(500).json({ error: 'Internal Server Error.' });
  }
};
//---- get random products from database as featured ----//
export const getFeaturedProducts = async (
  request: Request,
  response: Response
) => {
  try {
    const products = await Product.aggregate([{ $sample: { size: 6 } }]);
    if (!products.length)
      return response.status(404).json({ message: 'No product found!' });
    response.status(200).json(products);
  } catch (error) {
    if (error instanceof Error)
      response.status(500).json({ error: 'Internal Server Error.' });
  }
};
//---- filtering products ----//
export const filterProducts = async (request: Request, response: Response) => {
  const { filters } = request.body;
  try {
    let filterArgs: any = {};
    let priceRange: PriceRange = { from: '0', to: '0' };
    if (filters.price.length > 0) {
      priceRange = filterByPriceRange(filters.price[0]);
    }
    for (let key in filters) {
      if (filters[key].length > 0) {
        filterArgs[key] = { $in: filters[key] };
        if (filters.price.length > 0) {
          filterArgs.price = { $gte: priceRange.from, $lte: priceRange.to };
        }
      }
    }
    const products = await Product.find(filterArgs);
    response.status(200).json(products);
  } catch (error) {
    if (error instanceof Error)
      response.status(500).json({ error: 'Internal Server Error.' });
  }
};
//---- search products ----//
export const searchProducts = async (request: Request, response: Response) => {
  const { searchQuery } = request.body;
  try {
    const products = await Product.find({ $text: { $search: searchQuery } });
    response.status(200).json(products);
  } catch (error) {
    if (error instanceof Error) console.log(error);
    response.status(500).json({ error: 'Internal Server Error.' });
  }
};
