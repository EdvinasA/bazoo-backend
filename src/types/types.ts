export interface PriceRange {
  from: string;
  to: string;
}
