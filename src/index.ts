import express, { Express } from 'express';
import dotenv from 'dotenv';
import fs from 'fs';
import path from 'path';
import cors from 'cors';
import connectDb from './db/dbconnect';

dotenv.config();

const app: Express = express();
const port = process.env.PORT || 3000;
const routePath = path.join(__dirname, './routes');

fs.readdirSync(routePath).forEach(async (filename) => {
  let route = path.join(routePath, filename);
  try {
    const item = await import(route);
    app.use('/api', item.default);
  } catch (error) {
    if (error instanceof Error) console.log(error.message);
  }
});
//database connection instance
connectDb();
//app instance
app.use(cors({ origin: true, credentials: true }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.listen(port, () => {
  console.log(`[server]: Server is running at http://localhost:${port}`);
});
