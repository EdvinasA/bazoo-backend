import mongoose from 'mongoose';

export default function connectDb() {
  const mongoURI = process.env.MONGO_DB || '';
  try {
    mongoose.connect(mongoURI, {});
  } catch (error) {
    if (error instanceof Error) console.error(error.message);
    process.exit(1);
  }
  const dbConnection = mongoose.connection;
  dbConnection.once('open', (_) => {
    console.log(`Database connected: ${mongoURI}`);
  });

  dbConnection.on('error', (error) => {
    if (error instanceof Error)
      console.error(`connection error: ${error} ${mongoURI}`);
  });
  return;
}
