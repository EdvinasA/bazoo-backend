import { PriceRange } from '../types/types';

export const filterByPriceRange = (price: string) => {
  let priceRange: PriceRange = { from: '0', to: '0' };
  switch (price) {
    case '$0 - $25':
      priceRange = { from: '0', to: '25' };
      break;
    case '$25 - $50':
      priceRange = { from: '25', to: '50' };
      break;
    case '$50 - $100':
      priceRange = { from: '50', to: '100' };
      break;
    case '$100 - $150':
      priceRange = { from: '100', to: '150' };
      break;
  }
  return priceRange;
};
